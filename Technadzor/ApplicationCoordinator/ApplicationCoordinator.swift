//
//  ApplicationCoordinator.swift
//  Technadzor
//
//  Created by Orlov Maxim on 28/10/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//
import UIKit

final class ApplicationCoordinator: BaseCoordinator {
    
    private let coordinatorFactory: CoordinatorsFabric
    private var window: UIWindow?
    private var launchInstructor = LaunchInstructor.configure()
    
    init(window: UIWindow?,
         coordinatorFactory: CoordinatorsFabric) {
        self.coordinatorFactory = coordinatorFactory
        self.window = window
    }
    
    override func start() { //Add cases for other flows
        switch launchInstructor {
        case .main: runMainFlow()
        }
    }
    
    private func runMainFlow() { //MARK: Run main flow
        let (coordinator, controller) = coordinatorFactory.createTabBarCoordinator()
        window?.rootViewController = controller.toPresent()
        window?.makeKeyAndVisible()
        addDependency(coordinator)
        coordinator.start()
    }
}

