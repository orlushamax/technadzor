//
//  LaunchInstructor.swift
//  Technadzor
//
//  Created by Orlov Maxim on 28/10/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//

import Foundation

enum LaunchInstructor { //MARK: Add another flows if needed
    case main
    
    static func configure() -> LaunchInstructor {
        return .main
    }
}
