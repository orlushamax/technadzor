//
//  ContactsCoordinator.swift
//  Technadzor
//
//  Created by Orlov Maxim on 10/11/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//


import UIKit

protocol ContactsCoordinator: AnyObject {
   
}

class ContactsCoordinatorImp: BaseCoordinator {
    
    var router: Router?
    var controllersFabric: ControllersFabric
    private let coordinatorsFabric: CoordinatorsFabric
    private let tabBarController: UITabBarController
    
    init(tabBarController: UITabBarController, controllerFabric: ControllersFabric, coordinatorFabric: CoordinatorsFabric) {
        self.controllersFabric = controllerFabric
        self.coordinatorsFabric = coordinatorFabric
        self.tabBarController = tabBarController
    }
    
    override func start() {
        let controller = TabBarBuilder(controllerFabric: controllersFabric).buildContactsViewController()
        let interactor = ContactsInteractorImp(controller: controller,
                                              coordinator: self,
                                              client: ContactsClientImp())
        controller.interactor = interactor
        let navController = UINavigationController(rootViewController: controller)
        self.router = RouterImp(rootController: navController)
        tabBarController.viewControllers?.append(navController)
    }
}

extension ContactsCoordinatorImp: ContactsCoordinator {
    
   
}
