//
//  ContactsInteractor.swift
//  Technadzor
//
//  Created by Orlov Maxim on 10/11/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//

import Foundation

protocol ContactsInteractor {
    
}

class ContactsInteractorImp: ContactsInteractor {
    
    private weak var controller: ContactsViewControllerType?
    private let coordinator: ContactsCoordinator
    private let client: ContactsClient
    
    private var privateUserInfoResponse: UserInfo?
    
    init(controller: ContactsViewControllerType,
         coordinator: ContactsCoordinator,
         client: ContactsClient) {
        self.controller = controller
        self.client = client
        self.coordinator = coordinator
    }
    
    func requestData() {
        
    }
 
}
