//
//  ContactsViewController.swift
//  Technadzor
//
//  Created by Orlov Maxim on 10/11/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//

import UIKit

protocol ContactsViewControllerType: AnyObject {
    
}

class ContactsViewController: BaseViewController {

    var interactor: ContactsInteractor!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = "Контакты"
    }
}

extension ContactsViewController: ContactsViewControllerType {
    
}
