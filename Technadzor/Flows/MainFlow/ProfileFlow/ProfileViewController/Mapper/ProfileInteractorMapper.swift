//
//  ProfileInteractorMapper.swift
//  Technadzor
//
//  Created by Orlov Maxim on 12/11/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//

import Foundation

protocol ProfileMapper {
    func mapProfileMenuItems(response: MenuItemsResponse) -> [ProfileMenuItem]
}

class ProfileMapperImp: ProfileMapper {
    
    func mapProfileMenuItems(response: MenuItemsResponse) -> [ProfileMenuItem] {
        var sections = [ProfileMenuItem]()
        let userInfo = ProfileMenuItem.userInfo(UserInfo(
            id: response.user.id,
            name: response.user.name,
            position: response.user.position,
            company: response.user.company,
            image: response.user.image)
        )
        sections.append(userInfo)
        
        let menuItems = mapMenuItems(items: response.menuItems)
        sections.append(contentsOf: menuItems)
        return sections
    }
    
    private func mapMenuItems(items: [MenuItem]) -> [ProfileMenuItem] {
        return items.map({ item -> ProfileMenuItem in
            return ProfileMenuItem.menuItem(MenuItemViewModel(
                id: item.id,
                title: item.title,
                nested: item.nested,
                image: item.image
            ))
        })
     }
}
