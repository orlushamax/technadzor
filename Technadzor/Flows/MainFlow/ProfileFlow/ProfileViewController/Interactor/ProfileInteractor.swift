//
//  ProfileInteractor.swift
//  Technadzor
//
//  Created by Orlov Maxim on 10/11/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//

import Foundation

protocol ProfileInteractor {
    func requestData()
    func didTapSettings()
    var menuItems: [ProfileMenuItem] { get }
}

class ProfileInteractorImp: ProfileInteractor {
    
    private weak var controller: ProfileViewControllerType?
    private let coordinator: ProfileCoordinator
    private let client: ProfileClient
    private let mapper: ProfileMapper
    
    var menuItems: [ProfileMenuItem] {
        get {
            return privateMenuItemsList
        }
    }
    
    private var privateMenuItemsList = [ProfileMenuItem]()
    
    init(
        controller: ProfileViewControllerType?,
        coordinator: ProfileCoordinator,
        client: ProfileClient,
        mapper: ProfileMapper
    ) {
        self.controller = controller
        self.coordinator = coordinator
        self.client = client
        self.mapper = mapper
    }
    
    func requestData() {
        client.requestProfileItems(params: nil, callback: { [weak self] (response, error) in
            guard let weakSelf = self else { return }
            if let response = response {
                weakSelf.privateMenuItemsList = weakSelf.mapper.mapProfileMenuItems(response: response)
                weakSelf.controller?.update()
            } else if let error = error {
                print(error)
            } else {
                print("Undefined client error")
            }
        })
    }
    
    func didTapSettings() {
        coordinator.didTapSettings()
    }
    

}
