//
//  ProfileModel.swift
//  Technadzor
//
//  Created by Orlov Maxim on 10/11/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//

import Foundation

enum ProfileMenuItem {
    case userInfo(UserInfo)
    case menuItem(MenuItemViewModel)
}

struct MenuItemsResponse: Decodable {
    let user: UserInfo
    let menuItems: [MenuItem]
}

struct MenuItem: Decodable {
    let id: Int
    let title: String
    let image: String?
    let nested: [NestedMenuItem]
}

struct NestedMenuItem: Decodable {
    let id: Int
    let title: String
    let image: String?
}

class MenuItemViewModel {
    let id: Int
    let title: String
    let nested: [NestedMenuItem]
    let image: String?
    var isUnfolded: Bool = false
    
    init(id: Int, title: String, nested: [NestedMenuItem], image: String?) {
        self.id = id
        self.title = title
        self.nested = nested
        self.image = image
    }
}

struct UserInfo: Decodable {
    let id: Int
    let name: String
    let position: String
    let company: String
    let image: String
}

struct MenuModel {
    let id: Int
    let title: String
    let image: String?
    let nested: [MenuNestedContainer]?
}

struct MenuNestedContainer {
      let id: Int
      let title: String
      let image: String?
}
