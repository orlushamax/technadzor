//
//  ProfileClient.swift
//  Technadzor
//
//  Created by Orlov Maxim on 10/11/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//

import Foundation

protocol ProfileClient {
    typealias ProfileItemsCallback = (_ result: MenuItemsResponse?, _ error: Error?) -> Void
    func requestProfileItems(params: [String : String]?, callback: @escaping ProfileItemsCallback)
}

class ProfileClientImp: ProfileClient {
    func requestProfileItems(params: [String : String]?, callback: @escaping ProfileItemsCallback) {
        NetworkMock.requestProfileItemsMock(callback: callback)
    }
}
