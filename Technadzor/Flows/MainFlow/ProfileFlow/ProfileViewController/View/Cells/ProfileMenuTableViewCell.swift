//
//  ProfileMenuTableViewCell.swift
//  Technadzor
//
//  Created by Orlov Maxim on 10/11/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//

import UIKit
import Kingfisher

class ProfileMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var menuImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.textColor = AppStyle.textMainColor
        separatorView.backgroundColor = AppStyle.separatorColor
        menuImage.backgroundColor = AppStyle.textMainColor
    }

    func configureWith(model: NestedMenuItem) {
        titleLabel.text = model.title
        menuImage.kf.setImage(with: URL(string: model.image ?? ""))
    }
}
