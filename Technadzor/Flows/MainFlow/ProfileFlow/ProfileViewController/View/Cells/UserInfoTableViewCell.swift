//
//  UserInfoTableViewCell.swift
//  Technadzor
//
//  Created by Orlov Maxim on 10/11/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//

import UIKit
import Kingfisher

class UserInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        backgroundColor = AppStyle.cellBackgroundColor
        
        userPhoto.layer.cornerRadius = userPhoto.bounds.width / 2
        userPhoto.clipsToBounds = true
        
        nameLabel.textColor = AppStyle.textMainColor
        nameLabel.font = UIFont.baseBoldFont18
        positionLabel.textColor = AppStyle.textAdditionalColor
        positionLabel.font = UIFont.baseFont16
        companyLabel.textColor = AppStyle.textMainColor
        companyLabel.font = UIFont.baseThinFont14
    }

    func configureSelfWith(model: UserInfo) {
        userPhoto.kf.setImage(with: URL(string: model.image))
        nameLabel.text = model.name
        companyLabel.text = model.company
        positionLabel.text = model.position
    }
}
