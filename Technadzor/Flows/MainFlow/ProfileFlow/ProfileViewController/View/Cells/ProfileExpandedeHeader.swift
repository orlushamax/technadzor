//
//  ProfileExpandedeHeader.swift
//  Technadzor
//
//  Created by Orlov Maxim on 10/11/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//

import UIKit

protocol ProfileExpandedDelegate: AnyObject {
    func toggleSection(section: Int)
}

class ProfileExpandedeHeader: UITableViewHeaderFooterView {

    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var disclosureIndicator: UIImageView!
    @IBOutlet weak var menuImage: UIImageView!
    @IBOutlet weak var separatorView: UIView!
    
    weak var delegate: ProfileExpandedDelegate?
    
    private var model: MenuItemViewModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapHeader)))
        headerLabel.textColor = AppStyle.textMainColor
        disclosureIndicator.image = AppStyle.disclosure
        menuImage.backgroundColor = AppStyle.textMainColor
        separatorView.backgroundColor = AppStyle.separatorColor
    }
    
    @objc private func didTapHeader() {
        guard let model = model else { return }
        model.isUnfolded = !model.isUnfolded
        disclosureIndicator?.rotate(model.isUnfolded ? .pi/2  : 0.0)
        headerLabel.textColor = model.isUnfolded ? AppStyle.textAdditionalColor : AppStyle.textMainColor
        delegate?.toggleSection(section: self.tag)
    }
    
    func configureSelfWith(model: MenuItemViewModel) {
        self.model = model
        headerLabel.text = model.title
        menuImage.kf.setImage(with: URL(string: model.image ?? ""))
    }

}
