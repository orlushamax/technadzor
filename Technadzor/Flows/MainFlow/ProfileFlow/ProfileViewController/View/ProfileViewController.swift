//
//  ProfileViewController.swift
//  Technadzor
//
//  Created by Orlov Maxim on 10/11/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//

import UIKit

protocol ProfileViewControllerType: AnyObject {
    func update()
}

class ProfileViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    var interactor: ProfileInteractor!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        interactor.requestData()
    }
    
    private struct Constants {
        static var userProfileHeight: CGFloat = 116 //Высота профиля
        static var sectionHeaderHeight: CGFloat = 50 //Высота хедера секции
        static var menuItemheight: CGFloat = 50
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = "Профиль"

        let rightBarButtonItem = UIBarButtonItem(image: AppStyle.settingsIcon, style: .done, target: self, action: #selector(didTapSettings))
        navigationItem.rightBarButtonItem = rightBarButtonItem
    }
    
    @objc private func didTapSettings() {
        interactor.didTapSettings()
    }
    
    private func setupUI() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedSectionHeaderHeight = 50
        tableView.separatorStyle = .none
        tableView.register(ProfileMenuTableViewCell.self)
        tableView.register(UserInfoTableViewCell.self)
        let nib = UINib(nibName: "ProfileExpandedeHeader", bundle: nil)
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: "ProfileExpandedeHeader")
    }
}

extension ProfileViewController: ProfileViewControllerType {
    func update() {
        tableView.reloadData()
    }
}

extension ProfileViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        switch interactor.menuItems[section] {
        case .userInfo(_ ):
            return CGFloat.leastNonzeroMagnitude
        case .menuItem(_ ):
            return Constants.sectionHeaderHeight
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch interactor.menuItems[section] {
        case .userInfo(_ ):
            return nil
        case .menuItem(let item):
            if let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ProfileExpandedeHeader") as? ProfileExpandedeHeader {
                headerView.configureSelfWith(model: item)
                headerView.tag = section
                headerView.delegate = self
                return headerView
            }
        }
        return UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return interactor.menuItems.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch interactor.menuItems[section] {
        case .userInfo(_ ):
            return 1
        case .menuItem(let item):
            return item.isUnfolded ? item.nested.count : 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = interactor.menuItems[indexPath.section]
        switch section {
        case .userInfo(let userInfoModel):
            let cell: UserInfoTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureSelfWith(model: userInfoModel)
            return cell
        case .menuItem(let menuItem):
            let cell: ProfileMenuTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            let model = menuItem.nested[indexPath.row]
            cell.configureWith(model: model)
            return cell
        }
    }

}

extension ProfileViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch interactor.menuItems[section] {
        case .userInfo(_ ):
            return CGFloat.leastNonzeroMagnitude
        case .menuItem(_ ):
            return Constants.sectionHeaderHeight
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch interactor.menuItems[section] {
        case .userInfo(_ ):
            return Constants.sectionHeaderHeight
        case .menuItem(_ ):
            return CGFloat.leastNonzeroMagnitude
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch interactor.menuItems[indexPath.section] {
        case .userInfo(_ ):
            return Constants.userProfileHeight
        case .menuItem(_ ):
            return Constants.menuItemheight
        }
    }
}

extension ProfileViewController: ProfileExpandedDelegate {
    func toggleSection(section: Int) {
        let menuSection = interactor.menuItems[section]
        switch menuSection{
        case .menuItem(let menuItem):
            tableView.beginUpdates()
            if menuItem.isUnfolded {
                for i in 0...menuItem.nested.count - 1 {
                    tableView.insertRows(at: [IndexPath.init(row: i, section: section)], with: .fade)
                }
            } else  {
                for i in 0...menuItem.nested.count - 1 {
                    tableView.deleteRows(at: [IndexPath.init(row: i, section: section)], with: .fade)
                }
            }
            tableView.endUpdates()
        default:
            return
        }
    }
    
}
