//
//  SettingsInteractor.swift
//  Technadzor
//
//  Created by Orlov Maxim on 12/11/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//

import Foundation

protocol SettingsInteractor {
    
}

class SettingsInteractorImp: SettingsInteractor {
    
    private weak var controller: SettingsViewControllerType?
    private let coordinator: ProfileCoordinator
    private let client: SettingsClient
    
    init(controller: SettingsViewControllerType?,
         coordinator: ProfileCoordinator,
         client: SettingsClient) {
        self.controller = controller
        self.client = client
        self.coordinator = coordinator
    }
}
