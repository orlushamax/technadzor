//
//  SettingsViewController.swift
//  Technadzor
//
//  Created by Orlov Maxim on 12/11/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//

import UIKit

protocol SettingsViewControllerType: AnyObject {
    
}

class SettingsViewController: BaseViewController {

    var interactor: SettingsInteractor!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = "Настройки"
    }
}

extension SettingsViewController: SettingsViewControllerType {
    
}
