//
//  ProfileCoordinator.swift
//  Technadzor
//
//  Created by Orlov Maxim on 10/11/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//

import UIKit

protocol ProfileCoordinator: AnyObject {
   func didTapSettings() 
}

class ProfileCoordinatorImp: BaseCoordinator {
    
    var router: Router?
    var controllersFabric: ControllersFabric
    private let coordinatorsFabric: CoordinatorsFabric
    private let tabBarController: UITabBarController
    
    init(tabBarController: UITabBarController, controllerFabric: ControllersFabric, coordinatorFabric: CoordinatorsFabric) {
        self.controllersFabric = controllerFabric
        self.coordinatorsFabric = coordinatorFabric
        self.tabBarController = tabBarController
    }
    
    override func start() {
        let controller = TabBarBuilder(controllerFabric: controllersFabric).buildProfileViewController()
        let interactor = ProfileInteractorImp(
            controller: controller,
            coordinator: self,
            client: ProfileClientImp(),
            mapper: ProfileMapperImp()
        )
        controller.interactor = interactor
        let navController = UINavigationController(rootViewController: controller)
        self.router = RouterImp(rootController: navController)
        tabBarController.setViewControllers([navController], animated: true)
    }
    
    func didTapSettings() {
        let controller = controllersFabric.createSettingsViewController()
        let ineractor = SettingsInteractorImp(
            controller: controller,
            coordinator: self,
            client: SettingsClientImp()
        )
        controller.interactor = ineractor
        self.router?.push(controller)
    }
}

extension ProfileCoordinatorImp: ProfileCoordinator {
    
   
}
