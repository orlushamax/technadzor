//
//  RemarksViewController.swift
//  Technadzor
//
//  Created by Orlov Maxim on 28/10/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//

import UIKit
import Kingfisher

protocol RemarksViewControllerType: AnyObject {
    func update() 
}

class RemarksViewController: BaseViewController {

    var interactor: RemarksInteractor!
    @IBOutlet private weak var tableView: UITableView!

    private struct Constants {
        static let cellWidth: CGFloat = UIScreen.main.bounds.width - 48
        static let cellHeight: CGFloat = UIScreen.main.bounds.width * 0.3
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        interactor.requestData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = "Проекты"
    }
    
    private func setupUI() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(RemarksTableViewCell.self)
    }
}

extension RemarksViewController: RemarksViewControllerType {
    func update() {
        tableView.reloadData()
    }
}

extension RemarksViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return interactor.response.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = interactor.response[indexPath.row]
        let cell: RemarksTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureSelfWith(model: model)
        return cell
    }
}

extension RemarksViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
}
