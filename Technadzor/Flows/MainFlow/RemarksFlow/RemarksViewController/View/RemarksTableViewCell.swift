//
//  RemarksTableViewCell.swift
//  Technadzor
//
//  Created by Orlov Maxim on 10/11/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//

import UIKit
import Kingfisher

class RemarksTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var baseImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        accessoryType = .disclosureIndicator
        baseImage.layer.cornerRadius = baseImage.bounds.width / 2
        baseImage.clipsToBounds = true
    }

    func configureSelfWith(model: ConstructionObject) {
        baseImage.kf.setImage(with: URL(string: model.image))
        titleLabel.text = model.title
    }
}
