//
//  RemarksInteractor.swift
//  Technadzor
//
//  Created by Orlov Maxim on 28/10/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//

import Foundation

protocol RemarksInteractor {
    func requestData()
    var response: [ConstructionObject] { get }
}

class RemarksInteractorImp: RemarksInteractor {
    
    private weak var controller: RemarksViewControllerType?
    private let coordinator: RemarksCoordinator
    private let client: RemarksClient
    
    var response: [ConstructionObject] {
        get {
            return privateResponse
        }
    }

    private var privateResponse = [ConstructionObject]()
    
    init(controller: RemarksViewControllerType?, coordinator: RemarksCoordinator, client: RemarksClient) {
        self.controller = controller
        self.client = client
        self.coordinator = coordinator
    }
    
    func requestData() {
        client.requestConstructionObjectList(params: nil, callback: {[weak self] (response, error) in
            guard let weakSelf = self else { return }
            if let response = response {
                weakSelf.privateResponse = response.list
                weakSelf.controller?.update()
            } else if let error = error {
                print(error)
            } else {
                print("Undefined client error")
            }
        })
    }
}
