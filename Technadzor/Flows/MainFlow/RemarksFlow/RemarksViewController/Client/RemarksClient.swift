//
//  RemarksClient.swift
//  Technadzor
//
//  Created by Orlov Maxim on 28/10/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//

import Foundation

protocol RemarksClient {
    typealias RemarksMainCallback = (_ result: ConstructionObjectListResponse?, _ error: Error?) -> Void
    func requestConstructionObjectList(params: [String: String]?, callback: @escaping RemarksMainCallback)
}

class RemarksClientImp: RemarksClient {
    
    func requestConstructionObjectList(params: [String : String]?, callback: @escaping RemarksMainCallback) {
         NetworkMock.requestRemarksMainMock(callback: callback)
    }
    
}
