//
//  RemarksModel.swift
//  Technadzor
//
//  Created by Orlov Maxim on 28/10/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//

import Foundation

struct ConstructionObjectListResponse: Decodable {
    let list: [ConstructionObject]
}

struct ConstructionObject: Decodable {
    let id: Int
    let image: String
    let title: String
}

