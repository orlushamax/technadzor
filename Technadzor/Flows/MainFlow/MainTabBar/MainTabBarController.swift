//
//  MainTabBarController.swift
//  Technadzor
//
//  Created by Orlov Maxim on 28/10/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//

import UIKit
import Kingfisher

final class MainTabBarController: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        configureTabBar()
        tabBar.barTintColor = AppStyle.tabBarBackgroundColor
        tabBar.tintColor = AppStyle.tabBarSelectedTextColor
        tabBar.unselectedItemTintColor = AppStyle.tabBarUnSelectedTextColor
        tabBar.isTranslucent = false
    }
    
    private func configureTabBar() {
        tabBar.isTranslucent = false
    }

}

