//
//  TabBarBuilder.swift
//  Technadzor
//
//  Created by Orlov Maxim on 28/10/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//

import UIKit
import Kingfisher

final class TabBarBuilder {
    
    private let controllersFabric: ControllersFabric
    
    init(controllerFabric: ControllersFabric) {
        self.controllersFabric = controllerFabric
    }
    
    func buildTabBarViewController() -> MainTabBarController {
        let tabBarController = controllersFabric.createMainTabBar()
        return tabBarController
    }
}

//MARK: General view controller
extension TabBarBuilder {
    
    func buildRemarksViewController() -> RemarksViewController {
        let viewController = controllersFabric.createRemarksViewController()
        let img = UIImage(named: "remarks")
        return buildTabViewController(viewController: viewController,
                                      tabTitle: nil,
                                      img: img,
                                      selectedImg: nil) as! RemarksViewController
    }
    
    func buildProfileViewController() -> ProfileViewController {
        let viewController = controllersFabric.createProfileViewController()
        let img = UIImage(named: "profile")
        return buildTabViewController(viewController: viewController,
                                      tabTitle: nil,
                                      img: img,
                                      selectedImg: nil) as! ProfileViewController
    }
    
    func buildContactsViewController() -> ContactsViewController {
        let viewController = controllersFabric.createContactsViewController()
        let img = UIImage(named: "contacts")
        return buildTabViewController(viewController: viewController,
                                      tabTitle: nil,
                                      img: img,
                                      selectedImg: nil) as! ContactsViewController
    }
}

//MARK: Configurations for each tab view controller
extension TabBarBuilder {
    
    private func buildTabViewController(viewController: UIViewController,
                                        tabTitle: String?,
                                        img: UIImage?,
                                        selectedImg: UIImage?) -> UIViewController {
        
        viewController.tabBarItem = UITabBarItem(title: tabTitle,
                                                 image: img,
                                                 selectedImage: selectedImg)
        return viewController
    }
}




















