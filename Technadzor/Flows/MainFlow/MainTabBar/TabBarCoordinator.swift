//
//  TabBarCoordinator.swift
//  Technadzor
//
//  Created by Orlov Maxim on 28/10/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//

import UIKit

class TabBarCoordinator: BaseCoordinator {
    
    private let tabBarController: MainTabBarController
    private let coordinatorFabric: CoordinatorsFabric
    
    init(tabbarController: MainTabBarController, coordinatorsFabric: CoordinatorsFabric) {
        self.tabBarController = tabbarController
        self.coordinatorFabric = coordinatorsFabric
    }
    
    override func start() {
        runProfileFlow()
        runRemarksFlow()
        runContacsFlow()
    }
    
    private func runRemarksFlow() {
        let coordinator = coordinatorFabric.createRemarksCoordinator(tabBarController: tabBarController)
        coordinator.start()
        addDependency(coordinator)
    }
    
    private func runProfileFlow() {
        let coordinator = coordinatorFabric.createProfileCoordinator(tabBarController: tabBarController)
        coordinator.start()
        addDependency(coordinator)
    }
    
    private func runContacsFlow() {
        let coordinator = coordinatorFabric.createContactsCoordinator(tabBarController: tabBarController)
        coordinator.start()
        addDependency(coordinator)
    }
}





















