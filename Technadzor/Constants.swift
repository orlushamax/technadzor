//
//  Constants.swift
//  Technadzor
//
//  Created by Orlov Maxim on 28/10/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//
//https://icons8.ru/ иконки
//https://convertingcolors.com/rgb-color-236_173_0.html тут можно конвертировать цвета

import UIKit

class AppStyle {
    
    //Colors
    static let backGroundColor = UIColor.backGroundColor
    static let textMainColor = UIColor.textMainColor
    static let textAdditionalColor = UIColor.textAdditionalColor
    static let cellBackgroundColor = UIColor.cellBackgroundColor
    static let separatorColor = UIColor.separatorColor //Цвет разделителей
    
    //Images
    static let disclosure = UIImage.disclosure //Картинка для >
    
    //NavBar
    static let navBarBackGroundColor = UIColor.navBarBackGroundColor //Цвет фона навигационника
    static let navBarTextColor = UIColor.navBarTextColor //Цвет текста навигационника
    static let style: UIStatusBarStyle = .lightContent //Цвет стаус бара белый / черный
    
    //Settings
    static let settingsIcon = UIImage.settingsIcon //Картинка для настроек
    
    //TabBar
    static let tabBarSelectedTextColor = UIColor.tabBarSelectedTextColor
    static let tabBarUnSelectedTextColor = UIColor.tabBarUnSelectedTextColor
    static let tabBarBackgroundColor = UIColor.tabBarBackgroundColor
}

extension UIColor {
    
    static var tabBarSelectedTextColor: UIColor {
        return UIColor(hexString: "#0000")
    }
    
    static var tabBarUnSelectedTextColor: UIColor {
        return UIColor(hexString: "#909090")
    }
    
    static var tabBarBackgroundColor: UIColor {
        return UIColor(hexString: "#FFFFFF")
    }
    
    static var backGroundColor: UIColor {
        return UIColor(hexString: "#FFFFFF")
    }
    
    static var cellBackgroundColor: UIColor {
        return UIColor(hexString: "#E7E7E7")
    }
    
    static var textMainColor: UIColor {
        return UIColor(hexString: "#909090")
    }
    
    static var textAdditionalColor: UIColor {
        return UIColor(hexString: "#555555")
    }
    
    static var separatorColor: UIColor {
        return UIColor(hexString: "#B5B5B5")
    }
    
    static var navBarBackGroundColor: UIColor {
        return UIColor(hexString: "#ECAD00")
    }
    
    static var navBarTextColor: UIColor {
        return UIColor(hexString: "#FFFFFF")
    }
}

extension UIImage {
    
    static var disclosure: UIImage? {
        return UIImage(named: "disclosure")
    }
    
    static var settingsIcon: UIImage? {
        return UIImage(named: "settings")
    }
}

extension UIFont {
    
    
    static var baseFont16 = UIFont(name: "HelveticaNeue", size: 16.0)!
    static var baseFont17 = UIFont(name: "HelveticaNeue", size: 17.0)!
    static var baseFont18 = UIFont(name: "HelveticaNeue", size: 18.0)!
    static var baseFont19 = UIFont(name: "HelveticaNeue", size: 19.0)!
    static var baseFont20 = UIFont(name: "HelveticaNeue", size: 20.0)!
    static var baseFont21 = UIFont(name: "HelveticaNeue", size: 21.0)!
    static var baseFont22 = UIFont(name: "HelveticaNeue", size: 22.0)!
    static var baseFont23 = UIFont(name: "HelveticaNeue", size: 23.0)!
    
    static var baseThinFont12 = UIFont(name: "HelveticaNeue-Thin", size: 12.0)!
    static var baseThinFont13 = UIFont(name: "HelveticaNeue-Thin", size: 13.0)!
    static var baseThinFont14 = UIFont(name: "HelveticaNeue-Thin", size: 14.0)!
    static var baseThinFont15 = UIFont(name: "HelveticaNeue-Thin", size: 15.0)!
    static var baseThinFont16 = UIFont(name: "HelveticaNeue-Thin", size: 16.0)!
    static var baseThinFont17 = UIFont(name: "HelveticaNeue-Thin", size: 17.0)!
    static var baseThinFont18 = UIFont(name: "HelveticaNeue-Thin", size: 18.0)!
    static var baseThinFont19 = UIFont(name: "HelveticaNeue-Thin", size: 19.0)!
    static var baseThinFont20 = UIFont(name: "HelveticaNeue-Thin", size: 20.0)!
    static var baseThinFont21 = UIFont(name: "HelveticaNeue-Thin", size: 21.0)!
    static var baseThinFont22 = UIFont(name: "HelveticaNeue-Thin", size: 22.0)!
    static var baseThinFont23 = UIFont(name: "HelveticaNeue-Thin", size: 23.0)!
    
    static var baseBoldFont16 = UIFont(name: "HelveticaNeue-Bold", size: 16.0)!
    static var baseBoldFont17 = UIFont(name: "HelveticaNeue-Bold", size: 17.0)!
    static var baseBoldFont18 = UIFont(name: "HelveticaNeue-Bold", size: 18.0)!
    static var baseBoldFont19 = UIFont(name: "HelveticaNeue-Bold", size: 19.0)!
    static var baseBoldFont20 = UIFont(name: "HelveticaNeue-Bold", size: 20.0)!
    static var baseBoldFont21 = UIFont(name: "HelveticaNeue-Bold", size: 21.0)!
    static var baseBoldFont22 = UIFont(name: "HelveticaNeue-Bold", size: 22.0)!
    static var baseBoldFont23 = UIFont(name: "HelveticaNeue-Bold", size: 23.0)!
}
