//
//  BaseViewController.swift
//  Technadzor
//
//  Created by Orlov Maxim on 12/11/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//


import UIKit

class BaseViewController: UIViewController {
    
    private var style: UIStatusBarStyle = AppStyle.style
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupStatusBarStyle()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
    }
    
     override var preferredStatusBarStyle: UIStatusBarStyle {
           return self.style
    }
    
    func setupStatusBarStyle() {
        setupNavBar()
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    private func setupNavBar() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        if let image = UIImage(named: "back") {
            let backButtonImage = image.stretchableImage(withLeftCapWidth: 0, topCapHeight: 10)
            navigationController?.navigationBar.backIndicatorImage = backButtonImage
            navigationController?.navigationBar.backIndicatorTransitionMaskImage = backButtonImage
        }
        
        self.navigationController?.navigationBar.barTintColor = AppStyle.navBarBackGroundColor
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = AppStyle.navBarTextColor
        self.navigationController?.navigationBar.titleTextAttributes = ([NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 20), NSAttributedString.Key.foregroundColor : AppStyle.navBarTextColor])
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        get {
            return .none
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }

}


extension UINavigationController {
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        if let lastVC = self.viewControllers.last {
            return lastVC.preferredStatusBarStyle
        }
        
        return .default
    }
}
