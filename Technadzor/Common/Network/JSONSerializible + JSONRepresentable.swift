//
//  JSONSerializible + JSONRepresentable.swift
//  Technadzor
//
//  Created by Orlov Maxim on 28/10/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//

import Foundation

protocol JSONRepresentable {
    var JSONRepresentation: AnyObject { get }
}

protocol JSONSerializable: JSONRepresentable {
    
}

extension JSONSerializable {
    
    var JSONRepresentation: AnyObject {
        
        var representation = [String: AnyObject]()
        for case let (label?, value) in Mirror(reflecting: self).children {
            switch value {
            case let value as JSONRepresentable:
                representation[label] = value.JSONRepresentation
            case let value as NSObject:
                if ((value as? [JSONSerializable]) != nil) {
                    var lista: [AnyObject] = [AnyObject]()
                    for item: JSONSerializable in value as! [JSONSerializable] {
                        lista.append((item.JSONRepresentation))
                    }
                    representation[label] = lista as AnyObject
                } else {
                    representation[label] = value
                }
            default:
                // Ignore any unserializable properties
                break
            }
        }
        return representation as AnyObject
    }
}

extension JSONSerializable {
    
    func toJSON() -> String? {
        let representation = JSONRepresentation
        guard JSONSerialization.isValidJSONObject(representation) else {
            return nil
        }
        do {
            let data = try JSONSerialization.data(withJSONObject: representation, options: [])
            return String(data: data, encoding: String.Encoding.utf8)
        } catch {
            return nil
        }
    }
}
