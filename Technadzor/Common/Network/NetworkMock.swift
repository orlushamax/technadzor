//
//  NetworkMock.swift
//  Technadzor
//
//  Created by Orlov Maxim on 28/10/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//


import Foundation

// MARK: Mock of network
public final class NetworkMock {
    
    static var mockFlag: Bool {
        get {
            return true
        }
    }
    private struct Constants {
        static let remarksMain = "remarksMain"
        static let userInfo = "userInfo"
        static let profileItems = "profileMenuItems"
    }
    
    // Remarks
    static func requestRemarksMainMock(callback: @escaping RemarksClient.RemarksMainCallback) {
        guard let data = resourceMock(resource: Constants.remarksMain) else { callback(nil,nil); return}
        do {
            let model = try JSONDecoder().decode(ConstructionObjectListResponse.self, from: data)
            callback(model, nil)
        } catch {
            //TODO: Create valid error
            callback(nil, nil)
        }
    }
    
    // UserInfo
//    static func requestUserInfoMock(callback: @escaping ProfileClient.UserInfoCallback) {
//        guard let data = resourceMock(resource: Constants.userInfo) else { callback(nil,nil); return}
//        do {
//            let model = try JSONDecoder().decode(UserInfoResponse.self, from: data)
//            callback(model, nil)
//        } catch {
//            //TODO: Create valid error
//            callback(nil, nil)
//        }
//    }
    
    // ProfileMenuItems
    static func requestProfileItemsMock(callback: @escaping ProfileClient.ProfileItemsCallback) {
        guard let data = resourceMock(resource: Constants.profileItems) else { callback(nil,nil); return}
        do {
            let model = try JSONDecoder().decode(MenuItemsResponse.self, from: data)
            callback(model, nil)
        } catch {
            //TODO: Create valid error
            callback(nil, nil)
        }
    }

    private static func resourceMock(resource: String) -> Data? {
        guard let path = Bundle.main.path(forResource: resource , ofType: "json") else  { return nil }
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            return data
        } catch {
            return nil
        }
    }
}
