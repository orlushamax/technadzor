//
//  Network.swift
//  Technadzor
//
//  Created by Orlov Maxim on 28/10/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//

import Foundation
import Alamofire

public enum RequestMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case patch = "PATCH"
    case delete = "DELETE"
}

final class Network {
    
    private static var baseUrl:String {
        return "https://url"
    }
    
    private static let sessionManager = NetworkConfigurator.sessionManager()
    
    public static func request(requestMethod: RequestMethod, uri: String, params: [String: Any]?) -> DataRequest {
        
        let method = httpMethod(from: requestMethod)
        let url = baseUrl + uri
        return sessionManager.request(url, method: method, parameters: params, encoding: URLEncoding.default, headers: [:])
    }
    
    public static func request(requestMethod: RequestMethod, uri: String, params: [String: Any]?, headers: [String: String]?) -> DataRequest {
        
        let method = httpMethod(from: requestMethod)
        let url = baseUrl + uri
        let headers: HTTPHeaders? = headers
        
        
        return sessionManager.request(url, method: method, parameters: params, encoding: URLEncoding.default, headers: headers)
    }
    
    private static func httpMethod(from requestMethod: RequestMethod) -> HTTPMethod {
        return HTTPMethod(rawValue: requestMethod.rawValue) ?? .get
    }
}

final class NetworkConfigurator {
    
    public static func sessionManager() -> SessionManager {
        return Alamofire.SessionManager(configuration: configuration())
    }
    
    private static func headers() -> HTTPHeaders {
        var defaultHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        defaultHeaders["Accept"] = "application/json"
        return defaultHeaders
    }
    
    private static func configuration() -> URLSessionConfiguration {
        let conf = URLSessionConfiguration.default
        conf.httpAdditionalHeaders = headers()
        //        conf.timeoutIntervalForRequest = 10
        return conf
    }
}
