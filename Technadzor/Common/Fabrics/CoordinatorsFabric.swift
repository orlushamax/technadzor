//
//  CoordinatorsFabric.swift
//  Technadzor
//
//  Created by Orlov Maxim on 28/10/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//
import UIKit

protocol CoordinatorsFabric {
    func createTabBarCoordinator() -> (Coordinator, Presentable)
    func createRemarksCoordinator(tabBarController: UITabBarController) -> Coordinator
    func createProfileCoordinator(tabBarController: UITabBarController) -> Coordinator
    func createContactsCoordinator(tabBarController: UITabBarController) -> Coordinator 
}

class CoordinatorsFabricImp: CoordinatorsFabric {
    
    func createTabBarCoordinator() -> (Coordinator, Presentable) {
        let tabBar = TabBarBuilder(controllerFabric: ControllersFabricImp()).buildTabBarViewController()
        let tabBarCoordinator = TabBarCoordinator(tabbarController: tabBar,
                                                  coordinatorsFabric: CoordinatorsFabricImp())
        return (tabBarCoordinator,tabBar)
    }
    
    func createRemarksCoordinator(tabBarController: UITabBarController) -> Coordinator {
        let coordinator = RemarksCoordinatorImp(tabBarController: tabBarController,
                                                controllerFabric: ControllersFabricImp(),
                                                coordinatorFabric: CoordinatorsFabricImp())
        return coordinator
    }
    
    func createProfileCoordinator(tabBarController: UITabBarController) -> Coordinator {
        let coordinator = ProfileCoordinatorImp(tabBarController: tabBarController,
                                                controllerFabric: ControllersFabricImp(),
                                                coordinatorFabric: CoordinatorsFabricImp())
        return coordinator
    }
    
    func createContactsCoordinator(tabBarController: UITabBarController) -> Coordinator {
        let coordinator = ContactsCoordinatorImp(tabBarController: tabBarController,
                                                controllerFabric: ControllersFabricImp(),
                                                coordinatorFabric: CoordinatorsFabricImp())
        return coordinator
    }
}
