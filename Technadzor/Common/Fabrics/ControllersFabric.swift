//
//  ControllersFabric.swift
//  Technadzor
//
//  Created by Orlov Maxim on 28/10/2019.
//  Copyright © 2019 Orlov Maxim. All rights reserved.
//

import Foundation

protocol ControllersFabric {
    func createMainTabBar() -> MainTabBarController
    func createRemarksViewController() -> RemarksViewController
    func createProfileViewController() -> ProfileViewController
    func createContactsViewController() -> ContactsViewController
    func createSettingsViewController() -> SettingsViewController 
}

class ControllersFabricImp: ControllersFabric {
    
    func createMainTabBar() -> MainTabBarController {
        return MainTabBarController()
    }
    
    func createRemarksViewController() -> RemarksViewController {
        return RemarksViewController(nibName: String(describing: RemarksViewController.self), bundle: nil)
    }
    
    func createProfileViewController() -> ProfileViewController {
        return ProfileViewController(nibName: String(describing: ProfileViewController.self), bundle: nil)
    }
    
    func createContactsViewController() -> ContactsViewController {
        return ContactsViewController(nibName: String(describing: ContactsViewController.self), bundle: nil)
    }
    
    func createSettingsViewController() -> SettingsViewController {
        return SettingsViewController(nibName: String(describing: SettingsViewController.self), bundle: nil)
    }
}
